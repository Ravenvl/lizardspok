-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 21 2017 г., 17:14
-- Версия сервера: 5.5.53
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `spok`
--

-- --------------------------------------------------------

--
-- Структура таблицы `gamecalc`
--

CREATE TABLE `gamecalc` (
  `ID` int(5) UNSIGNED NOT NULL,
  `pl1` int(2) NOT NULL,
  `pl2` int(2) NOT NULL,
  `itog` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gamecalc`
--

INSERT INTO `gamecalc` (`ID`, `pl1`, `pl2`, `itog`) VALUES
(8, 1, 2, 1),
(9, 2, 1, 2),
(10, 1, 1, 0),
(11, 2, 2, 0),
(12, 3, 3, 0),
(13, 3, 1, 1),
(14, 1, 3, 2),
(15, 3, 2, 2),
(16, 2, 3, 1),
(17, 4, 4, 0),
(18, 5, 5, 0),
(19, 4, 1, 2),
(20, 4, 2, 2),
(21, 4, 3, 1),
(22, 4, 5, 1),
(23, 5, 1, 1),
(24, 5, 2, 1),
(25, 5, 3, 2),
(26, 5, 4, 2),
(27, 1, 4, 1),
(28, 1, 5, 2),
(29, 2, 4, 1),
(30, 2, 5, 2),
(31, 3, 4, 2),
(32, 3, 5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `games`
--

CREATE TABLE `games` (
  `ID` int(5) UNSIGNED NOT NULL,
  `IDroom` int(5) NOT NULL,
  `waypl1` int(11) DEFAULT NULL,
  `waypl2` int(11) DEFAULT NULL,
  `step` int(11) NOT NULL,
  `itog` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gamestatus`
--

CREATE TABLE `gamestatus` (
  `ID` int(5) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gamestatus`
--

INSERT INTO `gamestatus` (`ID`, `name`) VALUES
(0, 'Recruitment'),
(1, 'Recruitment'),
(2, 'Ready to start the game'),
(3, 'There is a game');

-- --------------------------------------------------------

--
-- Структура таблицы `rooms`
--

CREATE TABLE `rooms` (
  `ID` int(5) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `IDplayer1` int(5) NOT NULL,
  `IDplayer2` int(5) DEFAULT NULL,
  `IDstatus` int(5) NOT NULL,
  `maxtime` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `time`
--

CREATE TABLE `time` (
  `ID` int(10) UNSIGNED NOT NULL,
  `IDroom` int(11) NOT NULL,
  `gametime` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `userID` mediumint(8) UNSIGNED NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(150) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`userID`, `username`, `password`, `email`, `active`) VALUES
(1, 'user1', '12345', 'u1', 0),
(2, 'user2', '12345', 'u2', 1),
(3, 'user3', '12345', 'u3', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `gamecalc`
--
ALTER TABLE `gamecalc`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `gamestatus`
--
ALTER TABLE `gamestatus`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `IDplayer1` (`IDplayer1`),
  ADD UNIQUE KEY `IDplayer2` (`IDplayer2`);

--
-- Индексы таблицы `time`
--
ALTER TABLE `time`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `active` (`active`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `gamecalc`
--
ALTER TABLE `gamecalc`
  MODIFY `ID` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT для таблицы `games`
--
ALTER TABLE `games`
  MODIFY `ID` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;
--
-- AUTO_INCREMENT для таблицы `rooms`
--
ALTER TABLE `rooms`
  MODIFY `ID` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT для таблицы `time`
--
ALTER TABLE `time`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `userID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
